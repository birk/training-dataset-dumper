/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

#include "TriggerBTagMatcherAlg.h"
#include "AsgMessaging/MessageCheck.h"

#include <memory>


TriggerBTagMatcherAlg::TriggerBTagMatcherAlg(const std::string& name,
                                         ISvcLocator* pSvcLocator):
  AthReentrantAlgorithm(name, pSvcLocator)
{
  declareProperty("offlineBtagKey", m_offlineBtagKey);
  declareProperty("triggerBtagKey", m_triggerBtagKey);
  declareProperty("offlineJetKey", m_offlineJetKey);
  declareProperty("triggerJetKey", m_triggerJetKey);
  declareProperty("floatsToCopy", m_floats.toCopy);
  declareProperty("intsToCopy", m_ints.toCopy);
  declareProperty("jetIntsToCopy", m_jetInts.toCopy);
}

StatusCode TriggerBTagMatcherAlg::initialize() {
  ATH_CHECK(m_floats.initialize(this, m_offlineBtagKey, m_triggerBtagKey));
  ATH_CHECK(m_ints.initialize(this, m_offlineBtagKey, m_triggerBtagKey));
  ATH_CHECK(m_jetInts.initialize(
              this, m_offlineJetKey.key(), m_triggerJetKey.key()));
  m_triggerBtag = m_triggerBtagKey + ".jetLink";
  m_offlineBtag = m_offlineBtagKey + ".jetLink";
  m_drDecorator = m_triggerBtagKey + ".deltaRToOfflineJet";
  m_dPtDecorator = m_triggerBtagKey + ".deltaPtToOfflineJet";
  ATH_CHECK(m_triggerJetKey.initialize());
  ATH_CHECK(m_offlineJetKey.initialize());
  ATH_CHECK(m_triggerBtag.initialize());
  ATH_CHECK(m_offlineBtag.initialize());
  ATH_CHECK(m_drDecorator.initialize());
  ATH_CHECK(m_dPtDecorator.initialize());
  return StatusCode::SUCCESS;
}

namespace {
  using JL = ElementLink<xAOD::JetContainer>;
  using BC = xAOD::BTaggingContainer;
  std::vector<const xAOD::BTagging*> getBTagVector(
    SG::ReadDecorHandle<BC,JL>& handle) {
    std::vector<const xAOD::BTagging*> tags(handle->begin(), handle->end());
    std::sort(tags.begin(),tags.end(),[&h=handle](auto* t1, auto* t2){
      return (*h(*t1))->pt() > (*h(*t2))->pt();
    });
    return tags;
  }
}

StatusCode TriggerBTagMatcherAlg::execute(const EventContext& cxt) const {
  SG::ReadDecorHandle<BC,JL> trigJetGet(m_triggerBtag, cxt);
  SG::ReadDecorHandle<BC,JL> recoJetGet(m_offlineBtag, cxt);
  SG::WriteDecorHandle<BC,float> drDecorator(m_drDecorator, cxt);
  SG::WriteDecorHandle<BC,float> dPtDecorator(m_dPtDecorator, cxt);
  auto trigBTags = getBTagVector(trigJetGet);
  auto recoBTags = getBTagVector(recoJetGet);
  std::vector<MatchedPair<BC>> offTrigMatches;
  std::vector<MatchedPair<JC>> offTrigJetMatches;
  for (const xAOD::BTagging* trig: trigBTags) {
    auto& tjlink = trigJetGet(*trig);
    if (!tjlink.isValid()) throw std::runtime_error("bad trigger jet");
    const xAOD::Jet* tj = *tjlink;
    auto dr = [&h=recoJetGet, tj](const xAOD::BTagging& b) {
      return (*h(b))->p4().DeltaR(tj->p4());
    };
    auto minReco = std::min_element(
      recoBTags.begin(), recoBTags.end(),
      [&dr] (auto* t1, auto* t2) {
        return dr(*t1) < dr(*t2);
      });
    if (minReco == recoBTags.end()) {
      drDecorator(*trig) = NAN;
      dPtDecorator(*trig) = NAN;
      offTrigMatches.push_back({nullptr, trig});
      offTrigJetMatches.push_back({nullptr, tj});
    } else {
      const xAOD::BTagging* reco = *minReco;
      const xAOD::Jet* recoJet = *recoJetGet(*reco);
      drDecorator(*trig) = dr(*reco);
      dPtDecorator(*trig) = tj->pt() - recoJet->pt();
      offTrigMatches.push_back({reco, trig});
      offTrigJetMatches.push_back({recoJet, tj});
    }
  }
  m_floats.copy(offTrigMatches, cxt);
  m_ints.copy(offTrigMatches, cxt);
  m_jetInts.copy(offTrigJetMatches, cxt);

  return StatusCode::SUCCESS;
}
StatusCode TriggerBTagMatcherAlg::finalize () {
  return StatusCode::SUCCESS;
}
