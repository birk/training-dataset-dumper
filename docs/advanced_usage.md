The basic setup, described in the previous section, is capable of calculating simple variables from jets and b-tagging, and of running the neural network based flavor taggers. If you are starting from a DAOD that is likely all you need, **stop reading this and go to the basic section!** If you want to do something more complicated, read on.

The "advanced setup" supports a few additional workflows, e.g.:

- Augmenting release 21 (D)AODs to make them release 22 compatible,
- Extrapolating tracks to primary vertex to (re)calculate impact parameters
- Creating transient jet collections from trigger decisions, for
  trigger studies
- Building secondary vertices (i.e. "retagging") and running the full
  flavor tagging chain from untagged jets.

These workflows require you to use the Gaudi scheduler and event loop, part of the larger framework that ATLAS uses for trigger, reconstruction, and derivations.

For these cases we provide an Athena algorithm, `SingleBTagAlg`,
(technically a duel-use `EL::AnaAlgorithm`) which wraps the same
functionality as the stand alone executable.

### Installation

Further complicating matters, you can choose to build against either:

- `AthAnalysis`: the lightweight "analysis" Gaudi-based framework, or
- `Athena`: the full reconstruction framework

The lighter `AthAnalysis` framework will not support lower level reconstruction. For an exact list of which packages are included in every base project, see the `package_filters.txt` files under [the `Projects` directory in Athana][prj]. On the other hand, it's possible to run `AthAnalysis` locally via a docker image.

[prj]: https://gitlab.cern.ch/atlas/athena/-/tree/master/Projects

The training dataset dumper can be used within either release by using `setup/[project].sh` in place of
`setup/analysisbase.sh`. Make sure you rebuild your code _completely_ (delete the `build` directory) if you've changed releases!

Using the `Athena` project as an example, a local installation can be achieved with:

```bash
git clone ssh://git@gitlab.cern.ch:7999/atlas-flavor-tagging-tools/training-dataset-dumper.git
source training-dataset-dumper/setup/athena.sh
mkdir build
cd build
cmake ../training-dataset-dumper
make
cd ..
```

### Running

A very simple `ComponentAccumulator`-based script to run it is provided in [`ca-dump-single-btag`]({{repo_url}}-/blob/r22/BTagTrainingPreprocessing/bin/ca-dump-single-btag).
You can test it with

```bash
test-dumper ca
```

There are a number of other scripts matching `ca-*`: these are based around `ComponentAccumulator` Athena configuration.
Use the built in help (e.g. `ca-dump-single-btag -h`) to learn more about them.

### Retagging with locally modified Athena packages

In some cases, you may need to modify a part of the flavour-tagging full chain. For instance, you may want to tune one of the secondary vertexing algorithms to identify its best working point after some change occurred in Athena. To do that, you need a tool that allows you to re-run flavour tagging on your sample [importing the changes you made on Athena packages](development.md#editing-athena-packages).

[`ca-dump-retag`]({{repo_url}}-/blob/r22/BTagTrainingPreprocessing/bin/ca-dump-retag), the script that implements retagging, is such tool.

To use it, you need to setup your environment with `setup/athena.sh`.

The code runs as follows:
```bash
ca-dump-retag -c <path to configuration file> <paths to xAOD(s)>
```

To run on the grid, use:
```bash
grid-submit retag
```

### Using local lwtnn or ONNX file on grid
If you want to use a local lwtnn or ONNX model file to run inference on the grid you need to add the models to the tarball which is sent to the grid.
To do this, specify the network files in a comma separated list via the `-e` option of the `grid-submit` script.

On the grid, the models will be copied to the job working directory, so you should specify only the model filename for the  `nn_file_path` key in the `dl2_configs` block.

In order to have the same job run locally (for example to run a test before submission), place the model files in the top level of the directory you are working in with the dumper (i.e. in the directory which also contains the `build` and the `training-dataset-dumper` folder).


??? note "Alternative option"
    Alternatively, you can also use the folliwing approach.
    In order to do that you need to first create a `data` subfolder in the `BTagTrainingPreprocessing` directory
    ```bash
    mkdir -p BTagTrainingPreprocessing/data
    ```

    In addition, you need to add the following lines to the cmake file [`BTagTrainingPreprocessing/CMakeLists.txt`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/blob/r22/BTagTrainingPreprocessing/CMakeLists.txt#L172)
    ```
    # add files
    atlas_install_data(data/*)
    ```
    Before running the dumper, you need to recompile (inclduing `cmake`).

    Afterwards you copy all the network files (lwtnn, ONNX) into the directory `BTagTrainingPreprocessing/data`.
    As `nn_file_path` in the `dl2_configs` you need to specify then the following path `BTagTrainingPreprocessing/<network_file>` which will be found by the PathResolver.


### Running over heavy ion xAODs

You need to run under athanalysis (use `setup/athanalysis.sh`).

Since HI xAOD are still produced under R21 we can dump using:

```bash
ca-dump-upgrade-HI -c <path to configuration file> <paths to xAOD(s)>
```

The configuration file should be `upgrade-HI.json`, which will use `pflow-variables.json`. Flag `do_heavions` must be set to true in `upgrade-HI.json`.

To run on the grid use:
```bash
grid-submit upgrade-HI
```

### Running over r21.9 Upgrade AODs

As AODs for Upgrade configuration only exist in release 21.9 for now, running them with the Training Dataset Dumper requires a few modifications from the standard procedure.

The installation part is the same than the one described [here](installation.md) with the exception of the source part where the command `source training-dataset-dumper/setup/athena-latest.sh` must be used instead of `source training-dataset-dumper/setup/analysisbase.sh`.

Then, the command line to run the dumper is:

```
ca-dump-upgrade -c <path to configuration file> <paths to xAOD(s)>
```

The configuration files are located in `configs/single-btag` and the specific file for upgrade is named `upgrade_r21p9.json`.

An example of a ttbar r21.9 AOD sample is : `mc15_14TeV.600012.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.recon.AOD.e8185_s3695_s3700_r12714`.

### Producing trigger training datasets

You can make trigger training datasets in Athena with

```bash
ca-dump-trigger-pflow -c <dumper>/configs/trigger.json xAOD.root
```

This is a _much_ heavier job than the standard ones, because it has to
load in a number of dependencies for track extrapolation. As with the
other scripts, see the `-h` for more options.

You can use `grid-submit trigger` to submit these jobs to the grid.

### Working with nightlies

If you're on the bleeding edge of ATLAS software, there may not be a stable release that supports a new feature you need.
The scripts `setup/athena-latest.sh` and `setup/analysisbase-latest.sh` will set up the latest nightly build.

Working with nightlies comes with a few caveats:

- We make no promises that the code will work with them! Our continuous integration tests all updates in the tagged releases for both AnalysisBase and AthAnalysis based projects. This is not extended to nightlies.
- Nightlies will disappear after a few weeks. Because of this, **producing larger datasets** based on a nightly is **strongly discoursged:** no one will be able to reproduce your work when the nightly is deleted.
