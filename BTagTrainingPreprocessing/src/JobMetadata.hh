#ifndef JOB_METADATA_HH
#define JOB_METADATA_HH

#include <string>
#include <cmath>
#include <vector>
#include <memory>
#include <chrono>

namespace xAOD {
  class EventInfo_v1;
  using EventInfo = EventInfo_v1;
}

typedef std::chrono::steady_clock Clock;
typedef std::chrono::milliseconds milliseconds;
typedef std::chrono::duration<float, milliseconds::period> float_ms;

struct TimingStats {
  private:
    size_t n_jets;
    float sum_time; // ms
    float sq_sum_time; //ms^2

  public:
    std::string name;

    void update(float_ms time) {
      float time_ms = time.count();
      n_jets++;
      sum_time += time_ms;
      sq_sum_time += time_ms*time_ms;
    }

    std::tuple<size_t, float, float> get_stats() {
      float mean = sum_time / n_jets;
      float std_dev = std::sqrt(sq_sum_time / n_jets - mean * mean);
      float std_error = std_dev / std::sqrt(n_jets);
      return {n_jets, mean, std_error};
    }
};

void writeJobMetadata(
  const std::vector<std::shared_ptr<TimingStats>>&,
  const std::string& output_file = "userJobMetadata.json");

#endif
