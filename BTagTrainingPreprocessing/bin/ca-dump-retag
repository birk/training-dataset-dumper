#!/usr/bin/env python3

"""

Setup using setup/athena.sh is required for this script.

This runs "retagging" i.e. it overwrites the current b-tagging on jets
with new information. It has to run in Athena, to access track
extrapolators and secondary vertex reconstruction.

You are able to able to run the retagging over combined the LRT and
standard tracks with the -M option.

"""

from argparse import ArgumentParser
import sys

from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.MainServicesConfig import MainServicesCfg as getConfig
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg

from BTagTrainingPreprocessing import dumper
from BTagTrainingPreprocessing import retag as retag


def get_args():
    """
    Extend the base dumper argument parser.
    """
    parser = ArgumentParser(
        formatter_class=dumper.DumperHelpFormatter,
        parents=[dumper.base_parser(__doc__, add_help=False)],
    )
    track_opts = parser.add_mutually_exclusive_group()
    track_opts.add_argument(
        "-t",
        "--track-sys",
        help="list of tracking systematics to apply. For the full list of options\n"
             "see InDetTrackSystematicsTools/InDetTrackSystematics.h",
        nargs="+",
        type=str,
    )
    track_opts.add_argument(
        "-M",
        "--merge-tracks",
        action="store_true",
        help="merge the standard and large d0 track collections before everything else.\n"
             "only works in releases after 22.0.59 (2022-03-08T2101)",
    )
    return parser.parse_args()


def run():

    args = get_args()

    from AthenaConfiguration.AllConfigFlags import ConfigFlags as cfgFlags
    dumper.update_cfgFlags(cfgFlags, args)
    cfgFlags.lock()

    ca = getConfig(cfgFlags)
    ca.merge(PoolReadCfg(cfgFlags))
    ca.addService(CompFactory.AthenaEventLoopMgr(
        EventPrintoutInterval=args.event_print_interval))

    # add full blown retagging
    ca.merge(
        retag.retagging(
            cfgFlags,
            args.merge_tracks,
            "InDetTrackParticles",
            "AntiKt4EMPFlowJets",
            args.track_sys,
        )
    )

    # add dumper
    ca.merge(dumper.getDumperConfig(args))

    return ca.run()


if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)



