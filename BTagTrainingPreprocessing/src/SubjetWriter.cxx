#include "SubjetWriter.hh"
#include "BTagJetWriterConfig.hh"

#include "BTagJetWriterUtils.hh"
#include "SubstructureAccessors.hh"

#include "HDF5Utils/Writer.h"

// Less Standard Libraries (for atlas)
#include "H5Cpp.h"

// ATLAS things
#include "xAODJet/Jet.h"
#include "xAODEventInfo/EventInfo.h"

SubjetWriter::SubjetWriter(
  H5::Group& output_file,
  const SubjetWriterConfig& config)
{
  H5Utils::Compression c = H5Utils::Compression::STANDARD;

  // create the variable fillers
  JetConsumers fillers;

  add_jet_variables(fillers, config, H5Utils::Compression::HALF_PRECISION);
  add_event_info(fillers, config.event_info, c);

  if (config.write_kinematics_relative_to_parent) {
    add_parent_fillers(fillers);
  }

  m_hdf5_jet_writer.reset(
    new JetOutputWriter1D(output_file, config.name, fillers, config.n_subjets_to_save));
}

SubjetWriter::~SubjetWriter() {
  if (m_hdf5_jet_writer) m_hdf5_jet_writer->flush();
}
SubjetWriter::SubjetWriter(SubjetWriter&&) = default;


void SubjetWriter::write_dummy() {
  if (m_hdf5_jet_writer){
    std::vector<JetOutputs> dummy;
    m_hdf5_jet_writer->fill(dummy);
  }
}

void SubjetWriter::write_with_parent(std::vector<const xAOD::Jet*> jets,
                                      const xAOD::Jet* parent) {
  std::vector<JetOutputs> jos;
  for (const auto* jet: jets) {
    JetOutputs jo;
    jo.jet = jet;
    jo.parent = parent;
    jo.event_info = 0;
    jos.push_back(jo);
  }
  m_hdf5_jet_writer->fill(jos);
}
