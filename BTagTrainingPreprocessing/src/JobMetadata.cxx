#include "JobMetadata.hh"
#include "TruthTools.hh"

#include "xAODEventInfo/EventInfo.h"

#include "nlohmann/json.hpp"

#include <fstream>
#include <sstream>


void writeJobMetadata(
  const std::vector<std::shared_ptr<TimingStats>>& timings,
  const std::string& output_file) {

  nlohmann::json metadata;

  metadata["tagger_timings"]["units"] = "milliseconds";
  for (const auto& timing : timings) {
    auto [n_jets, mean, std_error] = timing->get_stats();
    metadata["tagger_timings"][timing->name]["n_jets"] = n_jets;
    metadata["tagger_timings"][timing->name]["mean"] = mean;
    metadata["tagger_timings"][timing->name]["std_error"] = std_error;
  }
 
  std::ofstream metastream(output_file);
  metastream << metadata.dump(2);

}
