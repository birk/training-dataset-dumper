#!/usr/bin/env bash

# This script should not be sourced, we don't need anything in here to
# propigate to the surrounding environment.
if [[ $- == *i* ]] ; then
    echo "Don't source me!" >&2
    return 1
else
  # set the shell to exit if there's an error (-e), and to error if
  # there's an unset variable (-u)
    set -eu
fi

BREAK="----------------------------------------------"
###################################################
# Add some mode switches
###################################################
declare -A SCRIPTS_BY_MODE=(
    [single-btag]=dump-single-btag
    [retag]=ca-dump-retag
    [trigger]=ca-dump-trigger-pflow
    [trigger-wp]=ca-dump-trigger-workingpoints
    [trigger-all]=ca-dump-trigger-all
    [trigger-mc16]=ca-dump-trigger-mc16
    [trigger-val]=ca-dump-trigger-all
    [upgrade]=ca-dump-upgrade
    [upgrade-HI]=ca-dump-upgrade-HI
    [nntc]=ca-dump-single-btag-fixedcone
    [xbb]=dump-single-btag
)
declare -A CONFIGS_BY_MODE=(
    [single-btag]=EMPFlow.json
    [retag]=EMPFlow.json
    [trigger]=trigger_pflow.json
    [trigger-wp]=trigger_wp.json
    [trigger-all]=trigger_all.json
    [trigger-mc16]=trigger_mc16.json
    [trigger-val]=trigger_all.json
    [upgrade]=upgrade_r21p9.json
    [upgrade-HI]=upgrade-HI.json
    [nntc]=EMPFlow_fixedcone.json
    [xbb]=FatJets.json
)
declare -A INPUTS_BY_MODE=(
    [single-btag]=single-btag.txt
    [retag]=single-btag.txt
    [trigger]=trigger.txt
    [trigger-wp]=trigger-workingpoints.txt
    [trigger-all]=trigger.txt
    [trigger-mc16]=trigger-mc16.txt
    [trigger-val]=trigger-val.txt
    [upgrade]=upgrade.txt
    [upgrade-HI]=upgrade-HI.txt
    [nntc]=single-btag.txt
    [xbb]=xbb.txt
)
###################################################
# CLI
###################################################
_usage() {
    echo "usage: ${0##*/} [-h] [options] MODE"
}
_help() {
    _usage
    cat <<EOF

    Submit dumper jobs to the grid! This script will ask you to commit your changes.
    Specify a running MODE (below) and then optionally overwrite the mode's defaults
    using the optional flags. You are encoraged to use the -t argument to tag your
    submission.

Options:
 -s script : Executable to be run (e.g. dump-single-btag)
 -c config : Path to the config file to use for the jobs
 -i file   : File listing input samples, will override the default list
 -t tag    : Tag the current code state using the supplied string
 -f        : Force, don't require changes to be committed
 -m        : Use --forceStage prun flag
 -a        : Run a test job using only one file per dataset
 -d        : Dry run, don't submit anything or make a tarball, but build the
             submit directory
 -e        : external files to be added to grid job

Modes:
$(for key in "${!CONFIGS_BY_MODE[@]}"; do
    echo -e " $key   \t=> ${SCRIPTS_BY_MODE[$key]} -c ${CONFIGS_BY_MODE[$key]}";
done)

EOF
}

# defaults
SCRIPT=""; CONFIG=""; INPUT_DATASETS=""; EXT_FILES="";
TAG=""; FORCE=""; DRYRUN=""; EXTRA_ARGS=""; ANNOTATE="";
while getopts ":hs:c:i:t:e:fmad" opt $@;
do
    case $opt in
        h) _help; exit 1;;
        s) SCRIPT=${OPTARG};;
        c) CONFIG=${OPTARG};;
        i) INPUT_DATASETS=${OPTARG};;
        t) TAG=${OPTARG};;
        e) EXT_FILES=${OPTARG};;
        f) FORCE=1 ;;
        m) EXTRA_ARGS+=' --forceStaged ';;
        a) EXTRA_ARGS+=' --nFiles 1 '; ANNOTATE+=.test ;;
        d) DRYRUN="echo DRY RUNNING: " ;;
        # handle errors
        \?) _usage; echo "Unknown option: -$OPTARG" >&2; exit 1;;
        :)  _usage; echo "Missing argument for -$OPTARG" >&2; exit 1;;
        *)  _usage; echo "Unimplemented option: -$OPTARG" >&2; exit 1;;
    esac
done
shift $((OPTIND-1))

# check required args
if [[ "$#" -ne 1 ]]; then
    echo "ERROR: Please specify a running mode after optional arguments" >&2
    _usage
    exit 1
fi
MODE=$1

if [[ -z ${SCRIPTS_BY_MODE[$MODE]+foo} ]]; then
    echo "ERROR: Invalid mode! Run ${0##*/} -h for allowed modes" >&2
    exit 1
fi

if [[ -n ${FORCE} && -n ${TAG} ]]; then
    echo "ERROR: Invalid args! Using -t with -f is not supported." >&2
    exit 1
fi

# this is where all the source files are
BASE=$(realpath --relative-to=$PWD $(dirname $(readlink -e ${BASH_SOURCE[0]}))/../..)
CONFIG_DIR=${BASE}/configs
INPUTS_DIR=${BASE}/BTagTrainingPreprocessing/grid/inputs
WORK_DIR=$PWD

# if arguments are not specified, use mode
if [[ -z "$SCRIPT" ]]; then SCRIPT=${SCRIPTS_BY_MODE[$MODE]}; fi
if [[ -z "$CONFIG" ]]; then CONFIG=$CONFIG_DIR/${CONFIGS_BY_MODE[$MODE]}; fi
if [[ -z "$INPUT_DATASETS" ]]; then INPUT_DATASETS=$INPUTS_DIR/${INPUTS_BY_MODE[$MODE]}; fi
if [[ "$DRYRUN" ]]; then echo -e $BREAK '\nDRY RUNNING'; fi

# let the user know what options we are using
echo $BREAK
echo -e "Script\t: $SCRIPT"
echo -e "Config\t: $CONFIG"
echo -e "Inputs\t: $INPUT_DATASETS"
echo -e "External files\t: $EXT_FILES"
echo $BREAK

###################################################
# Check for early exit
###################################################

# check arguments
if [[ ! -f $CONFIG ]]; then echo "Config file doesn't exist!"; exit 1; fi
if [[ ! -f $INPUT_DATASETS ]]; then echo "Inputs file doesn't exist!"; exit 1; fi

# check for panda setup
if ! type prun &> /dev/null ; then
    echo "ERROR: You need to source the grid setup script before continuing!" >&2
    exit 1
fi

# check to make sure you've properly set up the environemnt: if you
# haven't sourced the setup script in the build directory the grid
# submission will fail, so we check here before doing any work.
if ! type $SCRIPT &> /dev/null ; then
    echo "ERROR: Code setup with the wrong release or you haven't sourced build/x*/setup.sh" >&2
    exit 1
fi

# check for uncontrolled changes
if [[ ! $FORCE ]]; then
    if ! git -C ${BASE} diff-index --quiet HEAD; then
        echo "ERROR: uncommitted changes, please commit them" >&2; exit 1
    fi
fi

###################################################
# Some variable definitions
###################################################
# users's grid name
GRID_NAME=${RUCIO_ACCOUNT-${USER}}

######################################################
# Create an identifier for this job
######################################################
if [[ ${TAG} ]]; then
    cd ${BASE}

    ORIGIN_URL=$(git remote get-url origin)
    if [[ $ORIGIN_URL != *"$GRID_NAME"* ]]; then
        echo "ERROR: Remote URL for \"origin\" is not a fork, please submit from a fork" >&2
        exit 1
    fi

    DUMP_ID=$(git tag --points-at)
    if [[ -z ${DUMP_ID} ]]; then
        DUMP_ID=$(date +%y-%m-%d)_${TAG}
        echo "No tag found, creating tag ${DUMP_ID}"
        ${DRYRUN} git tag ${DUMP_ID} -m "automated tdd submission tag $(date +%F-T%H:%M:%S)"
    else
        echo "Found and resusing tag ${DUMP_ID}"
    fi
    cd ${WORK_DIR}
elif git -C ${BASE} diff-index --quiet HEAD; then
    DUMP_ID=$(git -C ${BASE} describe)
    echo "You didn't tag the code :(, consider using -t next time"
else
    echo "You are forcing a submission with uncontrolled changes! Using datestamp as job ID"
    DUMP_ID=$(date +%y-%m-%d-T%H%M%S)
fi

######################################################
# Prep the submit area
######################################################
# this is the subdirectory we submit from
SUBMIT_DIR=submit

echo "Preparing submit directory"
if [[ -d ${SUBMIT_DIR} ]]; then
    echo "Removing old submit directory"
    rm -rf ${SUBMIT_DIR}
fi

mkdir ${SUBMIT_DIR}

# write the expanded config file to the submit dir
test-config-merge $CONFIG > ${SUBMIT_DIR%/}/${CONFIG##*/}

if [ -n "${EXT_FILES}" ]; then
    # copying all files as external files
    IFS=',' read -r -a EXT_FILES_ARR <<< "${EXT_FILES}"

    for element in "${EXT_FILES_ARR[@]}"
    do
    cp -r "${element}" "${SUBMIT_DIR}/${element}"
    done
fi

cd ${SUBMIT_DIR}
# build a zip of the files we're going to submit
ZIP=job.tgz
echo "Making tarball of local files: ${ZIP}" >&2

# the --outTarBall, --noSubmit, and --useAthenaPackages arguments are
# important. The --outDS and --exec don't matter at all here, they are
# just placeholders to keep panda from complianing.
PRUN_ARGS="--outTarBall=${ZIP} --noSubmit --useAthenaPackages \
--exec='ls' --outDS=user.${GRID_NAME}.x"

# check if ${EXT_FILES} is not empty and append it to the default args
if [ -n "${EXT_FILES}" ]; then
    PRUN_ARGS="${PRUN_ARGS} --extFile=${EXT_FILES}"
fi

# now run the script with the prun args
${DRYRUN} prun ${PRUN_ARGS}

######################################################
# Loop over datasets and submit
######################################################
# parse inputs file
INPUT_DATASETS=$(grep -v '^#'  ${WORK_DIR}/$INPUT_DATASETS)
INPUT_DATASETS=($INPUT_DATASETS)

# loop over all inputs
echo $BREAK
$DRYRUN
echo "Submitting ${#INPUT_DATASETS[*]} datasets as ${GRID_NAME}..."
echo $BREAK

# define a fucntion to do all the submitting
function submit-job() (
    set -eu
    DS=$1

    # this regex extracts the DSID from the input dataset name, so
    # that we can give the output dataset a unique name. It's not
    # pretty: ideally we'd just suffix our input dataset name with
    # another tag. But thanks to insanely long job options names we
    # use in the generation stage we're running out of space for
    # everything else.
    DSID=$(sed -r 's/[^\.]*\.([0-9]{6,8})\..*/\1/' <<< ${DS})

    # build the full output dataset name
    CONFIG_FILE=${CONFIG##*/}

    # this terrible regex extracts the atlas tags, e.g. e4342_s3443...
    ATLAS_TAGS=$(egrep -o '(_?[esdfarp][0-9]{3,6}){3,}' <<< ${DS})
    TAGS=${ATLAS_TAGS}.tdd.${CONFIG_FILE%.*}
    if [[ -n $AtlasVersion ]]; then
        TAGS=${TAGS}.${AtlasVersion//./_}
    else
        TAGS=${TAGS}.${AtlasBuildStamp}
    fi
    OUT_DS=user.${GRID_NAME}.${DSID}.${TAGS}.${DUMP_ID}${ANNOTATE}

    # check to make sure the grid name isn't too long
    if [[ $(wc -c <<< ${OUT_DS}) -ge 120 ]] ; then
        echo "ERROR: dataset name ${OUT_DS} is too long, can't submit!" 1>&2
        return 1
    fi

    # now submit
    printf "${DS} \n\t-> ${OUT_DS}\n"
    ${DRYRUN} prun --exec "${SCRIPT} %IN -c ${CONFIG_FILE}"\
         --outDS ${OUT_DS} --inDS ${DS}\
         --useAthenaPackages --inTarBall=${ZIP}\
         --mergeScript="hdf5-merge-nolock -o %OUT -i %IN"\
         --outputs output.h5\
         --noEmail \
         ${EXTRA_ARGS} > ${OUT_DS}.log 2>&1
)

# we have to export some environment variables so xargs can read them
export -f submit-job
export CONFIG GRID_NAME DUMP_ID ZIP AtlasBuildStamp SCRIPT DRYRUN EXTRA_ARGS ANNOTATE EXT_FILES

# use xargs to submit all these jobs in batches of 10
printf "%s\n" ${INPUT_DATASETS[*]} | xargs -P 10 -I {} bash -c "submit-job {}"
echo $BREAK
echo "Submission successful"

######################################################
# Push tags if necessary
######################################################
if [[ ${TAG} ]]; then
    echo "Pushing tag ${DUMP_ID}"
    cd ../${BASE}
    ORIGIN_URL=$(git remote get-url origin)
    if [[ $ORIGIN_URL != *"$GRID_NAME"* ]]; then
        echo "ERROR: Remote URL for \"origin\" is not a fork, please submit from a fork" >&2
        exit 1
    fi
    ${DRYRUN} git push -q origin ${DUMP_ID}
    cd ${WORK_DIR}
fi
echo $BREAK
