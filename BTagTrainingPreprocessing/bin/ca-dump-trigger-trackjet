#!/usr/bin/env python3

"""
Dumper for VR track jets that are reconstructed in trigger
"""

import sys
from BTagTrainingPreprocessing import dumper, trigger, mctc
from JetTagCalibration.JetTagCalibConfig import JetTagCalibCfg
from BTagging.BTagRun3Config import BTagAlgsCfg
from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator

def PoorMansIpAug(name, prefix, trackContainer, primaryVertexContainer):
    ca = ComponentAccumulator()
    augmenter = CompFactory.FlavorTagDiscriminants.PoorMansIpAugmenterAlg(
        name = name,
        prefix = prefix,
        trackContainer = trackContainer,
        primaryVertexContainer = primaryVertexContainer)
    ca.addEventAlgo(augmenter)
    return ca

def run():

    args = dumper.base_parser(__doc__).parse_args()

    from AthenaConfiguration.AllConfigFlags import ConfigFlags as cfgFlags
    dumper.update_cfgFlags(cfgFlags, args)
    cfgFlags.lock()

    ca = dumper.getMainConfig(cfgFlags, args)
    fs_tracks = 'HLT_IDTrack_FS_FTF'
    fs_vertices = 'HLT_IDVertex_FS'
    labelAlg = trigger.getLabelingBuilderAlg(cfgFlags)
    DRTool = trigger.getDRTool(cfgFlags)
    ca.addEventAlgo(labelAlg)
    ca.addEventAlgo(CompFactory.JetDecorationAlg(
        "HLT_TrackJetDeltaRLabelingAlg",
        Decorators = [DRTool],
        JetContainer = "HLT_AntiKtVR30Rmax4Rmin02PV0TrackJets"
    ))

    ca.merge(mctc.getMCTC())

    #ca.addEventAlgo(CompFactory.FlavorTagDiscriminants.PoorMansIpAugmenterAlg(
    #    name = "PoorMansIpAugmenter_HLT_IDTrack_FS_FTF",
    #    prefix = 'btagIp_',
    #    trackContainer = fs_tracks,
    #    primaryVertexContainer = fs_vertices))
    ca.merge(PoorMansIpAug(
        name = "PoorMansIpAugmenter_HLT_IDTrack_FS_FTF",
        prefix = 'btagIp_',
        trackContainer = fs_tracks,
        primaryVertexContainer = fs_vertices))
    ca.merge(JetTagCalibCfg(cfgFlags))
    ca.merge(BTagAlgsCfg(
        cfgFlags,
        JetCollection="HLT_AntiKtVR30Rmax4Rmin02PV0TrackJets",
        nnList=['BTagging/20211216trig/dips/AntiKt4EMPFlow/network.json'],
        trackCollection= fs_tracks,
        primaryVertices= fs_vertices,
        muons='',
        renameTrackJets = False,
        AddedJetSuffix=''
    ))

    ca.merge(dumper.getDumperConfig(args))
    return ca.run()

if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)
