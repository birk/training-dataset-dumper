The training dataset dumper is a tool which processes xAOD derivations and provides h5 ntuples as output format.
These ntuples are used in the FTAG group for the training and performance evaluation of algorithms and dedicated studies.
Information about centralised ntuple productions is maintained [here][ntup].
The [central FTAG docs][ftagdocs] also lists recent updates and talks.

Note that there are **many** different ways to use this code, please skim the [basic](basic_usage.md) and [advanced](advanced_usage.md) usage sections before deciding how to get started.


### Getting the code

The training dataset dumper is hosted on gitlab:

- [{{repo_url}}]({{repo_url}})

Docker images are available on CERN GitLab container registry:

- [CERN GitLab container registry]({{repo_url}}container_registry)


### Hands on tutorials (external)

For ATLAS members, a hands on tutorial on [getting started][tut1] working with the dumper is hosted on the FTAG Aglorithms docs page.
More advanced usage is covered in a second tutorial on [Athena algorithms][tut2].

[tut1]: https://ftag-docs.docs.cern.ch/software/tutorials/tutorial-tdd/
[tut2]: https://ftag-docs.docs.cern.ch/software/tutorials/tutorial-algorithms/

### Getting Help

If you have questions that aren't covered here, please post them [on AtlasTalk][at], we'll try to respond as quickly as possible.

For more interactive help, consider [joining][mmj] the [ATLAS Flavor Tagging mattermost team][mm]

[ntup]: https://ftag.docs.cern.ch/software/samples/
[ftagdocs]: https://ftag.docs.cern.ch/software/tdd/
[at]: https://atlas-talk.web.cern.ch/c/ftag
[mm]: https://mattermost.web.cern.ch/aft-algs/channels/h5-dumper
[mmj]: https://mattermost.web.cern.ch/signup_user_complete/?id=ektad7hj4fdf5nehdmh1js4zfy
