#include "SingleBTagTools.hh"
#include "SingleBTagConfig.hh"
#include "BTagJetWriterConfig.hh"
#include "SubjetWriter.hh"

#include "JetWriters/JetLinkedFlowWriter.h"

#include "FlavorTagDiscriminants/GNN.h"
#include "FlavorTagDiscriminants/HbbTag.h"
#include "FlavorTagDiscriminants/HbbTagConfig.h"
#include "FlavorTagDiscriminants/AssociationEnums.h"

#include "H5Cpp.h"

namespace {
  std::vector<std::string> get(const VariableList& v, const std::string& k) {
    if (v.count(k)) return v.at(k);
    return {};
  }
  void check_rc(StatusCode code) {
    if (!code.isSuccess()) throw std::runtime_error("bad return code");
  }

  BTagJetWriterConfig jwConfig(const SingleBTagConfig& jobcfg) {
    BTagJetWriterConfig jet_cfg;
    jet_cfg.event_info = get(jobcfg.btag, "event");
    jet_cfg.event_compressed = get(jobcfg.btag, "event_compressed");
    jet_cfg.char_variables = get(jobcfg.btag, "chars");
    jet_cfg.uchar_variables = get(jobcfg.btag, "uchars");
    jet_cfg.jet_int_variables = get(jobcfg.btag, "jet_int_variables");
    jet_cfg.jet_float_variables = get(jobcfg.btag,"jet_floats");
    jet_cfg.jet_half_variables = get(jobcfg.btag,"jet_halves");
    jet_cfg.jet_char_variables = get(jobcfg.btag,"jet_chars");
    jet_cfg.int_as_float_variables = get(jobcfg.btag, "ints_as_float");
    jet_cfg.int_as_half_variables = get(jobcfg.btag, "ints_as_halves");
    jet_cfg.half_variables = get(jobcfg.btag, "halves");
    jet_cfg.float_variables = get(jobcfg.btag, "floats");
    jet_cfg.double_variables = get(jobcfg.btag, "doubles");
    jet_cfg.variable_maps.replace_with_defaults_checks
      = jobcfg.default_flag_mapping;
    jet_cfg.variable_maps.rename = {}; // please don't use this :(
    jet_cfg.n_jets_per_event = 0;
    jet_cfg.name = "jets";
    jet_cfg.btagging_link = jobcfg.btagging_link;
    jet_cfg.force_full_precision = jobcfg.force_full_precision;
    return jet_cfg;
  }

  SubjetWriterConfig subjetWriterConfig(const SubjetConfig& jobcfg) {
    SubjetWriterConfig subjet_cfg;
    subjet_cfg.write_kinematics_relative_to_parent = true;
    subjet_cfg.double_variables = get(jobcfg.variables, "doubles");
    subjet_cfg.float_variables = get(jobcfg.variables, "floats");
    subjet_cfg.char_variables = get(jobcfg.variables, "chars");
    subjet_cfg.jet_int_variables = get(jobcfg.variables, "jet_int_variables");
    subjet_cfg.jet_float_variables = get(jobcfg.variables, "jet_floats");
    subjet_cfg.n_subjets_to_save = jobcfg.n_subjets_to_save;
    subjet_cfg.btagging_link = jobcfg.btagging_link;
    subjet_cfg.name = jobcfg.output_name;
    return subjet_cfg;
  }

  template<typename T>
  std::function<void(const T&)> makeGNNWrapper(
    const std::string& path,
    const std::map<std::string, std::string>& remapping,
    FlavorTagDiscriminants::TrackLinkType tlt,
    FlavorTagDiscriminants::FlipTagConfig ftc,
    std::shared_ptr<TimingStats> timing)
  {
    FlavorTagDiscriminants::GNN gnn(path, ftc, remapping, tlt);
    return [gnn, timing](const T& j) {
      auto start = Clock::now();
      gnn.decorate(j);
      auto stop = Clock::now();
      timing->update(stop - start);
    };
  }

  template<typename T>
  std::function<void(const T&)> makeXbbWrapper(
    const std::string& path,
    std::shared_ptr<TimingStats> timing)
  {
    FlavorTagDiscriminants::HbbTagConfig cfg(path);
    // need to hardcode subjet collection`name for now
    cfg.subjet_link_name = "GhostAntiKtVR30Rmax4Rmin02PV0TrackJets";
    // xbb needs to be copy constructable so create a shared_ptr to it
    auto xbb_ptr = std::make_shared<FlavorTagDiscriminants::HbbTag>(cfg);

    return [xbb_ptr, timing](const T& j) {
      auto start = Clock::now();
      xbb_ptr->decorate(j);
      auto stop = Clock::now();
      timing->update(stop - start);
    };
  }

  template <>
  std::function<void(const xAOD::BTagging&)> makeXbbWrapper<>( 
    const std::string&,
    std::shared_ptr<TimingStats>)
  {
    throw std::logic_error("The Xbb tagger must be called on a jet");
  }


  template<typename T>
  std::function<void(const T&)> makeDL2Wrapper(
    const std::string& path,
    const std::map<std::string, std::string>& remapping,
    FlavorTagDiscriminants::TrackLinkType tlt,
    FlavorTagDiscriminants::FlipTagConfig ftc,
    std::shared_ptr<TimingStats> timing)
  {
    FlavorTagDiscriminants::DL2HighLevel dl2(path, ftc, remapping, tlt);
    return [dl2, timing](const T& j) {
      auto start = Clock::now();
      dl2.decorate(j);
      auto stop = Clock::now();
      timing->update(stop - start);
    };
  }

  template<typename T>
  std::pair<std::shared_ptr<TimingStats>, std::function<void(const T&)>> makeNNWrapper(
    const DL2Config& cfg,
    FlavorTagDiscriminants::TrackLinkType tlt)
  {
    using E = DL2Config::Engine;
    const auto& path = cfg.nn_file_path;
    const auto& ftc = cfg.flip_tag_config;
    auto timing = std::make_shared<TimingStats>();
    timing->name = path;
    switch (cfg.engine) {
      case E::DL2: return {timing, makeDL2Wrapper<T>(path, cfg.remapping, tlt, ftc, timing)};
      case E::GNN: return {timing, makeGNNWrapper<T>(path, cfg.remapping, tlt, ftc, timing)};
      case E::XBB: return {timing, makeXbbWrapper<T>(path, timing)};
      default: throw std::logic_error("unknown engine");
    }
  }
}

TrackToolkit::TrackToolkit(const TrackConfig& cfg, H5::Group& output):
  selector(cfg.selection, cfg.input_name),
  sorted(trackSort(cfg.sort_order, cfg.selection.ip_prefix)),
  writer(output, cfg.writer),
  n_tracks_decorator("n_" + cfg.writer.name)
{
}
TrackToolkit::TrackToolkit(TrackToolkit&&) = default;

SubjetToolkit::SubjetToolkit(const SubjetConfig &init_cfg, H5::Group& output)
    : cfg(init_cfg),
      subjet_acc(init_cfg.input_name),
      parent_acc("Parent"),
      jet_writer(output, subjetWriterConfig(cfg)),
      n_subjets("n_" + init_cfg.output_name) {}
std::vector<const xAOD::Jet*> SubjetToolkit::getSubjets(const xAOD::Jet* jet){
  const xAOD::Jet* parent_jet = *parent_acc(*jet);
  std::vector<const xAOD::Jet*> subjets;
  auto subjet_links = subjet_acc(*parent_jet);
  for (const auto& el : subjet_links) {
    const auto* sjet = dynamic_cast<const xAOD::Jet*>(*el);
    if (!sjet) throw std::logic_error("subjet is invalid");
    if (sjet->pt() < cfg.min_jet_pt ||
        std::abs(sjet->eta()) > cfg.max_abs_eta)
      continue;
    if (sjet->numConstituents() < cfg.num_const) continue;
    subjets.push_back(sjet);
  }
  std::sort(
      subjets.begin(), subjets.end(),
      [](const auto* j1, const auto* j2) { return j1->pt() > j2->pt(); });
  return subjets;
}


SingleBTagTools::Accessors::Accessors(const SingleBTagConfig& cfg):
  eventClean_looseBad("DFCommonJets_eventClean_LooseBad"),
  jvt("Jvt")
{
  if (cfg.btagging_link.empty())
    btaggingLink = [] (const xAOD::Jet&) { return nullptr; };
  else{
    SG::AuxElement::ConstAccessor<ElementLink<xAOD::BTaggingContainer>>a(cfg.btagging_link);
    btaggingLink = [a] (const xAOD::Jet & j) { return *a(j); };
  }
}

SingleBTagTools::SingleBTagTools(const SingleBTagConfig& jobcfg):
  calibration_tool(jobcfg.tool_prefix + "JetCalibrationTool"),
  cleaning_tool(jobcfg.tool_prefix + "JetCleaningTool",
                JetCleaningTool::LooseBad, false),
#ifndef DISABLE_JVT
  jvttool(jobcfg.tool_prefix + "JetVertexTaggerTool"),
#endif
  shallow_copier(jobcfg.btagging_link),
  muon_augmenter("Muons"),
  track_classifier(jobcfg.tool_prefix + "TrackClassifierTool"),
  acc(jobcfg),
  trkTruthDecorator(""),
  trkLeptonDecorator("", jobcfg.tool_prefix)
{
  if (jobcfg.calibration){
    const JetCalibrationConfig& cfg = *jobcfg.calibration;
    JetCalibrationTool& jct = calibration_tool;
    check_rc( jct.setProperty("JetCollection", cfg.collection));
    check_rc( jct.setProperty("ConfigFile", cfg.configuration) );
    check_rc( jct.setProperty("CalibSequence", cfg.seq) );
    check_rc( jct.setProperty("CalibArea", cfg.area) );
    check_rc( jct.setProperty("IsData", false) );
    check_rc( jct.initialize() );
  }
  check_rc( cleaning_tool.setProperty("JetContainer",
                                      jobcfg.jet_collection) );
  check_rc( cleaning_tool.initialize() );

  check_rc( jvttool.setProperty("JetContainer",
                                jobcfg.jet_collection) );
  check_rc( jvttool.initialize() );

  if (!jobcfg.nntc.empty()) {
    FlavorTagDiscriminants::TrackClassifier& tct = track_classifier;
    check_rc( tct.setProperty("NNModelFilepath", jobcfg.nntc) );
    check_rc( tct.initialize() );
  }
  for (const auto& cfg: jobcfg.dl2_configs) {
    using FlavorTagDiscriminants::FlipTagConfig;
    using DL2 = FlavorTagDiscriminants::DL2HighLevel;
    using TLT = FlavorTagDiscriminants::TrackLinkType;
    const TLT tpl = TLT::TRACK_PARTICLE;
    const TLT ipl = TLT::IPARTICLE;
    std::string path = cfg.nn_file_path;
    if (cfg.where == DL2Config::Where::BTAG) {
      auto [timing, nn] = makeNNWrapper<xAOD::BTagging>(cfg, tpl);
      dl2s.push_back(nn);
      timings.push_back(timing);
    } else if (cfg.where == DL2Config::Where::JET) {
      auto [timing, nn] = makeNNWrapper<xAOD::Jet>(cfg, ipl);
      jet_nns.push_back(nn);
      timings.push_back(timing);
    }
  }

  // instantiate truth particle decorators
  for (const auto& cfg: jobcfg.truths) {
    if (cfg.selection) {
      jet_truth_associators.emplace_back(
        cfg.association_name, *cfg.selection);
    }
    if (!cfg.merge.empty()) {
      jet_truth_mergers.emplace_back(cfg.merge, cfg.association_name);
    }
    // add overlap checks
    if (cfg.overlap_dr > 0) {
      SG::AuxElement::ConstAccessor<JetTruthAssociator::PartLinks> acc(
        cfg.association_name);
      overlap_checks.emplace_back(
        [acc, dr=cfg.overlap_dr](const xAOD::Jet& j) {
          for (const JetTruthAssociator::PartLink& tpl: acc(j)) {
            if (!tpl.isValid()) {
              throw std::runtime_error("invalid particle link");
            }
            const xAOD::IParticle* part = *tpl;
            if (part->p4().DeltaR(j.p4()) < dr) return true;
          }
          return false;
        });
    }
    if (cfg.decorate) {
      std::string out_name;
      if (cfg.output) { out_name = cfg.output->name; }
      else { out_name = cfg.association_name; }
      jetTruthSummaryDecorators.emplace_back(cfg.association_name, out_name);
    }
  }
  if (jobcfg.hits) {
    hit_decorator.reset(new HitDecorator(jobcfg.hits->decorator));
  }
}
SingleBTagTools::~SingleBTagTools() = default;

SingleBTagOutputs::SingleBTagOutputs(const SingleBTagConfig& jobcfg,
                                     H5::Group& output):
  jet_writer(output, jwConfig(jobcfg))
{
  for (const SubjetConfig &cfg : jobcfg.subjet_configs) {
    subjets.emplace_back(cfg, output);
  }
  for (const TrackConfig& cfg: jobcfg.tracks) {
    tracks.emplace_back(cfg, output);
  }
  for (const TruthConfig& cfg: jobcfg.truths) {
    if (cfg.output) {
      const TruthOutputConfig& oc = *cfg.output;
      // TODO: TruthWriter needs a config struct
      truths.emplace_back(output, oc.n_to_save,
                          cfg.association_name, oc.name,
                          oc.sort_order);
    }
  }
  if (jobcfg.hits && jobcfg.hits->writer.output_size > 0) {
    hits.reset(new HitWriter(output, jobcfg.hits->writer));
  }

  if (jobcfg.flow) {
    flow.reset(new JetLinkedFlowWriter(output, *jobcfg.flow));
  }
}

SingleBTagOutputs::~SingleBTagOutputs() = default;
