#ifndef CLEANHITS_H
#define CLEANHITS_H

namespace xAOD {
  class TrackMeasurementValidation_v1;
  using TrackMeasurementValidation = TrackMeasurementValidation_v1;
  using TrackMeasurementValidationContainer =
    DataVector<TrackMeasurementValidation_v1>;
}

bool isGoodHit(const xAOD::TrackMeasurementValidation*);
std::vector<const xAOD::TrackMeasurementValidation*> cleanHits(const xAOD::TrackMeasurementValidationContainer*, bool); 

#endif
