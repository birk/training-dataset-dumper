#include "BTagTrackWriter.hh"

#include "JetWriters/JetTrackWriter.h"

// Less Standard Libraries (for atlas)
#include "H5Cpp.h"

// ATLAS things
#include "xAODTracking/TrackParticle.h"
#include "xAODJet/Jet.h"

///////////////////////////////////
// Class definition starts here
///////////////////////////////////
BTagTrackWriter::BTagTrackWriter(
  H5::Group& output_file,
  const JetConstituentWriterConfig& cfg):
  m_writer(new JetTrackWriter(output_file, cfg))
{
}

BTagTrackWriter::~BTagTrackWriter() = default;
BTagTrackWriter::BTagTrackWriter(BTagTrackWriter&&) = default;

void BTagTrackWriter::write(const BTagTrackWriter::Tracks& tracks,
                            const xAOD::Jet& jet) {
  m_writer->write(jet, tracks);
}
